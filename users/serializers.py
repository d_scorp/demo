from rest_framework import serializers
from users.models import Designation, UserData, UserControlTable, ClientTable, ClientType


class DesignationSerializers(serializers.ModelSerializer):
    class Meta:
        model = Designation
        fields = ['id', 'designation_value']


class UserSerializers(serializers.ModelSerializer):
    designation_id = DesignationSerializers()
    user_password = serializers.CharField(
        write_only=True,
        required=True,
        help_text='Leave empty if no change needed',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    def create(self, validated_data):
        user_password = validated_data.pop('user_password')
        user = super().create(validated_data)
        user.set_password(user_password)
        user.save()
        return user

    class Meta:
        model = UserData
        fields = ['id', 'user_name', 'user_email', 'user_password', 'user_contact', 'user_address', 'designation_id']


class UserControlTableSerializers(serializers.ModelSerializer):
    supervisor = UserSerializers()
    supervised = UserSerializers()

    class Meta:
        model = UserControlTable
        fields = ['id', 'supervisor', 'supervised']


class ClientTypeSerializers(serializers.ModelSerializer):
    class Meta:
        model = ClientType
        fields = ['id', 'client_type_name']


class ClientTableSerializers(serializers.ModelSerializer):
    client_type_id = ClientTypeSerializers()
    user_id = UserSerializers()
    updated_by = UserSerializers()

    class Meta:
        model = ClientTable
        fields = ['id', 'client_name', 'client_address', 'client_contact_no', 'client_date_of_birth', 'client_aniversary_date', 'client_occupation', 'client_type_id', 'user_id', 'updated_by', 'updated_date']
