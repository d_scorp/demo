from django.db import models

# Create your models here.

class ProductType(models.Model):
    product_type_name = models.CharField(max_length=255, null=False)

    def __str__(self):
        return self.product_type_name


class ProductInformation(models.Model):
    product_name = models.CharField(max_length=255, null=False)
    product_price = models.FloatField(null=False)
    product_type = models.ForeignKey(ProductType, on_delete=models.CASCADE, related_name='pi_product_type')

    def __str__(self):
        return self.product_name
    