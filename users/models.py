from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser


# Create your models here.

class Designation(models.Model):
    designation_value = models.CharField(max_length=255)

    def __str__(self):
        return self.designation_value
    

# class UserData(models.Model):
class UserData(AbstractUser):
    user_name = models.CharField(max_length=255, unique=True)
    user_email = models.EmailField(unique=True)
    user_password = models.CharField(max_length=255)
    user_contact = models.CharField(max_length=255)
    user_address = models.CharField(max_length=255)
    designation_id = models.ForeignKey(Designation, on_delete=models.CASCADE, related_name='user_designation_id')

    USERNAME_FIELD = 'user_name'
    REQUIRED_FIELDS = ['user_email']

    def __str__(self):
        return self.user_name


class UserControlTable(models.Model):
    supervisor = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='uct_supervisor')
    supervised = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='uct_supervised')


class ClientType(models.Model):
    client_type_name = models.CharField(max_length=255)

    def __str__(self):
        return self.client_type_name


class ClientTable(models.Model):
    client_name = models.CharField(max_length=255)
    client_address = models.CharField(max_length=255)
    client_contact_no = models.CharField(max_length=255)
    client_date_of_birth = models.CharField(max_length=255)
    client_aniversary_date = models.CharField(max_length=255)
    client_occupation = models.CharField(max_length=255)

    client_type_id = models.ForeignKey(ClientType, on_delete=models.CASCADE)
    user_id = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='ct_user')
    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='ct_updated_by')

    updated_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.client_name
