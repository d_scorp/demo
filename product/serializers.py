from rest_framework import serializers
from product.models import ProductInformation, ProductType


class ProductTypeSerializers(serializers.ModelSerializer):
    class Meta:
        model = ProductType
        fields = ['id', 'product_type_name']


class ProductInformationSerializers(serializers.ModelSerializer):
    product_type = ProductTypeSerializers()
    
    class Meta:
        model = ProductInformation
        fields = ['id','product_name', 'product_price', 'product_type']
