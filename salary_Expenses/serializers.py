from rest_framework import serializers
from users.serializers import UserSerializers
from salary_Expenses.models import SalaryExpenses


class SalaryExpensesSerializers(serializers.ModelSerializer):
    updated_by = UserSerializers()
    created_by = UserSerializers()

    class Meta:
        model = SalaryExpenses
        fields = ['id', 'date', 'salary', 'TA', 'DA', 'other', 'remarks', 'miscellaneous', 'visited_area', 'created_by', 'created_date', 'updated_by', 'updated_date', 'status1', 'status2']
        read_only_fields = ['created_by', 'updated_by']
    