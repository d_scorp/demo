from django.contrib import admin
from report_Generator.models import *


# Register your models here.
admin.site.register(LeaveReport)
admin.site.register(AdminReport)
admin.site.register(DailyCallReport)
admin.site.register(DcrHigher)

@admin.register(SalesReport)
class SalesReportAdmin(admin.ModelAdmin):
    list_display = ['report_date']
