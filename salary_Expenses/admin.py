from django.contrib import admin
from salary_Expenses.models import *


# Register your models here.
@admin.register(SalaryExpenses)
class SalaryExpensesAdmin(admin.ModelAdmin):
        list_display = ['date']
