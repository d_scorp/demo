from django.db import models
from users.models import UserData
from django.utils import timezone

# Create your models here.

class TourPlan(models.Model):
    date = models.DateField()
    morning_station = models.CharField(max_length=255)
    evening_station = models.CharField(max_length=255)
    morning_station_type = models.CharField(max_length=255)
    evening_station_type = models.CharField(max_length=255)

    creator_id = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='tp_creator')
    approver_id = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='tp_approver')

    status1 = models.CharField(max_length=255)
    status2 = models.CharField(max_length=255)

    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='tp_updated_by')

    updated_date = models.DateField(default=timezone.now)

    def __str__(self):
        return super().updated_by


class TourPlanHigher(models.Model):
    morning_supervised = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='tph_morning_supervised')
    evening_supervised = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='tph_evening_supervised')
    date = models.DateField()

    created_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='tph_created_by')
    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='tph_updated_by')

    updated_date = models.DateField(default=timezone.now)

    def __str__(self):
        return super().updated_by
    
