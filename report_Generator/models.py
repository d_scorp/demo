from django.db import models
from users.models import ClientTable, UserData, ClientType
from product.models import ProductInformation
from django.utils import timezone

# Create your models here.
class SalesReport(models.Model):
    report_date = models.DateField(null=False)

    client_name = models.ForeignKey(ClientTable, on_delete=models.CASCADE, related_name='sr_client_name')

    address = models.CharField(max_length=255)

    product = models.ForeignKey(ProductInformation, on_delete=models.CASCADE, related_name='sr_product')

    quantity = models.IntegerField(null=False)

    sales_person = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='sr_sales_person')
    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='sr_updated_by')

    updated_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.client_name


class DailyCallReport(models.Model):
    shift = models.CharField(max_length=255)
    visited_area = models.CharField(max_length=255)

    client_type = models.ForeignKey(ClientType, on_delete=models.CASCADE, related_name='dcr_client_type')
    client_name = models.ForeignKey(ClientTable, on_delete=models.CASCADE, related_name='dcr_client_name')

    gift = models.CharField(max_length=255)

    product = models.ForeignKey(ProductInformation, on_delete=models.CASCADE, related_name='dcr_product')

    quantity = models.IntegerField(null=False)
    amount = models.FloatField(null=False)

    mpo_id = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='dcr_mpo_id')

    dcr_date = models.DateField(max_length=255)
    created_date = models.DateField(default=timezone.now)

    visited_with = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='dcr_visited_with')
    sample = models.ForeignKey(ProductInformation, on_delete=models.CASCADE, related_name='dcr_sample')

    sample_qty = models.FloatField(null=False)

    def __str__(self):
        return self.client_name


class LeaveReport(models.Model):
    report_initiator = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='lr_report_initiator')
    report_receiver = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='lr_report_receiver')

    subject = models.TextField(max_length=512)
    status1 = models.CharField(max_length=255)
    status2 = models.CharField(max_length=255)

    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='lr_updated_by')
    
    updated_date = models.DateField(default=timezone.now)

    def __str__(self):
        return self.updated_by
    

class AdminReport(models.Model):
    report_writer = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='ar_report_writer')

    subject = models.TextField(max_length=512)

    report_supervisor = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='ar_report_supervisor')

    status1 = models.CharField(max_length=255)
    status2 = models.CharField(max_length=255)

    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='ar_updated_by')

    updated_date = models.DateField(default=timezone.now)

    def __str__(self):
        return self.report_writer


class DcrHigher(models.Model):
    date = models.DateField()
    supervised_id = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='dcr_high_supervised_id')

    dcr_id = models.ForeignKey(DailyCallReport, on_delete=models.CASCADE, related_name='dcr_high_dcr_id')

    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='dcr_high_updated_by')

    updated_date = models.DateField(default=timezone.now)

    def __str__(self):
        return super().updated_by
    