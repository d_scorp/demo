from django.contrib import admin
from tour_Plan_Generator.models import *

# Register your models here.
@admin.register(TourPlanHigher)
class TourPlanHigherAdmin(admin.ModelAdmin):
    list_display = ['date']

@admin.register(TourPlan)
class TourPlanAdmin(admin.ModelAdmin):
    list_display = ['date']
