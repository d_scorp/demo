from rest_framework import serializers
from tour_Plan_Generator.models import TourPlan, TourPlanHigher
from users.serializers import UserSerializers


class TourPlanSerializers(serializers.ModelSerializer):
    creator_id = UserSerializers()
    approver_id = UserSerializers()
    updated_by = UserSerializers()

    class Meta:
        model = TourPlan
        fields = ['id', 'date', 'morning_station', 'evening_station', 'morning_station_type', 'evening_station_type', 'creator_id', 'approver_id', 'status1', 'status2', 'updated_by', 'updated_date']
        read_only_fields = ['creator_id','updated_by', 'approver_id']


class TourPlanHigherSerializers(serializers.ModelSerializer):
    morning_supervised = UserSerializers()
    evening_supervised = UserSerializers()
    created_by = UserSerializers()
    updated_by = UserSerializers()

    class Meta:
        model = TourPlanHigher
        fields = ['id', 'morning_supervised', 'evening_supervised', 'created_by', 'updated_by', 'updated_date']
        read_only_fields = ['created_by','updated_by']