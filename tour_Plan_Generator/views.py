from django.shortcuts import render
from tour_Plan_Generator.serializers import *
from tour_Plan_Generator.models import *
from rest_framework import generics
from rest_framework.permissions import IsAdminUser
from rest_framework import status
from rest_framework.response import Response
import django_filters.rest_framework


# TourPlan class
class TourPlanListView(generics.ListAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['updated_by']


class TourPlanCreateView(generics.CreateAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanSerializers

    def create(self, request, **kwargs):
        if self.request.user.is_verified:
            serializer = TourPlanSerializers(data=request.data)
            if serializer.is_valid():
                serializer.save(creator_id=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class TourPlanDetailView(generics.RetrieveAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanSerializers
    # permission_classes = [IsAdminUser, ]
    lookup_field = 'pk'


class TourPlanUpdateView(generics.UpdateAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanSerializers
    lookup_field = 'pk'

    def update(self, request, **kwargs):
        if self.request.user.is_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            print(instance)
            serializer = TourPlanSerializers(instance, data=request.data, partial=partial)
            if serializer.is_valid():
                serializer.save(updated_by=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class TourPlanDeleteView(generics.DestroyAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanSerializers
    lookup_field = 'pk'


# TourPlanHigher class
class TourPlanHigherListView(generics.ListAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanHigherSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['updated_by']


class TourPlanHigherCreateView(generics.CreateAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanHigherSerializers

    def create(self, request, **kwargs):
        if self.request.user.is_verified:
            serializer = TourPlanHigherSerializers(data=request.data)
            if serializer.is_valid():
                serializer.save(creator_id=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class TourPlanHigherDetailView(generics.RetrieveAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanHigherSerializers
    # permission_classes = [IsAdminUser, ]
    lookup_field = 'pk'


class TourPlanHigherUpdateView(generics.UpdateAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanHigherSerializers
    lookup_field = 'pk'

    def update(self, request, **kwargs):
        if self.request.user.is_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            print(instance)
            serializer = TourPlanHigherSerializers(instance, data=request.data, partial=partial)
            if serializer.is_valid():
                serializer.save(updated_by=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class TourPlanHigherDeleteView(generics.DestroyAPIView):
    queryset = TourPlan.objects.all()
    serializer_class = TourPlanHigherSerializers
    lookup_field = 'pk'
