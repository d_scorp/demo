from django.db import models
from users.models import UserData
from django.utils import timezone

# Create your models here.

class SalaryExpenses(models.Model):
    date = models.DateField(null=False)
    salary = models.FloatField(null=False)
    TA = models.FloatField(null=False)
    DA = models.FloatField(null=False)
    other = models.FloatField()
    remarks = models.CharField(max_length=255)
    miscellaneous = models.FloatField()
    visited_area = models.CharField(max_length=255)

    created_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='se_created_by')

    created_date = models.DateField()

    updated_by = models.ForeignKey(UserData, on_delete=models.CASCADE, related_name='se_updated_by')

    updated_date = models.DateField(default=timezone.now)
    status1 = models.CharField(max_length=255)
    status2 = models.CharField(max_length=255)

    def __str__(self):
        return self.salary
    

