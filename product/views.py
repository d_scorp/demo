from product.models import *
from product.serializers import *
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser
import django_filters.rest_framework

# Create your views here.
class ProductTypeListView(generics.ListAPIView):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class ProductTypeCreateView(generics.CreateAPIView):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializers


class ProductTypeDetailView(generics.RetrieveAPIView):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializers
    # permission_classes = [IsAdminUser, ]
    lookup_field = 'pk'


class ProductTypeUpdateView(generics.UpdateAPIView):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializers
    # permission_classes = [IsAdminUser, ]
    lookup_field = 'pk'


class ProductTypeDestroyView(generics.DestroyAPIView):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializers
    lookup_field = 'pk'


class ProductInformationListView(generics.ListAPIView):
    queryset = ProductInformation.objects.all()
    serializer_class = ProductInformationSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class ProductInformationCreateView(generics.CreateAPIView):
    queryset = ProductInformation.objects.all()
    serializer_class = ProductInformationSerializers


class ProductInformationDetailView(generics.RetrieveAPIView):
    queryset = ProductInformation.objects.all()
    serializer_class = ProductInformationSerializers
    # permission_classes = [IsAdminUser, ]
    lookup_field = 'pk'


class ProductInformationUpdateView(generics.UpdateAPIView):
    queryset = ProductInformation.objects.all()
    serializer_class = ProductInformationSerializers
    lookup_field = 'pk'


class ProductInformationDeleteView(generics.DestroyAPIView):
    queryset = ProductInformation.objects.all()
    serializer_class = ProductInformationSerializers
    lookup_field = 'pk'
