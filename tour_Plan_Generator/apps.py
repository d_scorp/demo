from django.apps import AppConfig


class TourPlanGeneratorConfig(AppConfig):
    name = 'tour_Plan_Generator'
