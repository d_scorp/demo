from django.contrib import admin
from users.models import *


# Register your models here.
admin.site.register(UserData)
admin.site.register(Designation)
admin.site.register(ClientType)
@admin.register(ClientTable)
class ClientTableAdmin(admin.ModelAdmin):
    list_display = ['client_name','client_type_id','user_id','updated_by','updated_date']

@admin.register(UserControlTable)
class UserControlTableAdmin(admin.ModelAdmin):
    list_display = ['supervisor','supervised']
