from rest_framework import serializers
from users.serializers import UserSerializers, ClientTableSerializers, ClientTypeSerializers
from report_Generator.models import SalesReport, DailyCallReport, LeaveReport, AdminReport, DcrHigher
from product.serializers import ProductInformationSerializers


class SalesReportSerializers(serializers.ModelSerializer):
    updated_by = UserSerializers()
    client_name = ClientTableSerializers()
    product = ProductInformationSerializers()
    sales_person = UserSerializers()

    class Meta:
        model = SalesReport
        fields = ['id', 'report_date', 'client_name', 'address', 'product', 'quantity', 'sales_person', 'updated_by', 'updated_date']
        read_only_fields = ['updated_by']


class DailyCallReportSerializers(serializers.ModelSerializer):
    client_type = UserSerializers()
    client_name = UserSerializers()
    product = ProductInformationSerializers()
    mpo_id = UserSerializers()
    visited_with = UserSerializers()
    sample = ProductInformationSerializers()

    class Meta:
        model = DailyCallReport
        fields = ['id', 'shift', 'visited_area', 'client_type', 'client_name', 'gift', 'product', 'quantity', 'amount', 'mpo_id', 'dcr_date', 'created_date', 'visited_with', 'sample', 'sample_qty']
        # read_only_fields = []


class LeaveReportSerializers(serializers.ModelSerializer):
    updated_by = UserSerializers()
    report_initiator = UserSerializers()
    report_receiver = UserSerializers()

    class Meta:
        model = LeaveReport
        fields = ['id', 'report_initiator', 'report_receiver', 'subject', 'status1', 'status2', 'updated_by', 'updated_date']
        read_only_fields = ['upadated_by']


class AdminReportSerializers(serializers.ModelSerializer):
    updated_by = UserSerializers()
    report_writer = UserSerializers()
    report_supervisor = UserSerializers()

    class Meta:
        model = AdminReport
        fields = ['id', 'report_writer', 'subject', 'report_supervisor', 'status1', 'status2', 'updated_by', 'updated_date']
        read_only_fields = ['upadated_by']


class DcrHigherSerializers(serializers.ModelSerializer):
    dcr_id = DailyCallReportSerializers()
    updated_by = UserSerializers()

    class Meta:
        model = DcrHigher
        fields = ['id', 'date', 'supervised_id', 'dcr_id', 'updated_by', 'updated_date']
        read_only_fields = ['upadated_by']
