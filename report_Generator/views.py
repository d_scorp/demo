from report_Generator.models import *
from report_Generator.serializers import *
from rest_framework import generics, status
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
import django_filters.rest_framework

# Create your views here.
class LeaveReportListView(generics.ListAPIView):
    queryset = LeaveReport.objects.all()
    serializer_class = LeaveReportSerializers
    permission_classes = [IsAdminUser,]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['updated_by']


class LeaveReportCreateView(generics.CreateAPIView):
    queryset = LeaveReport.objects.all()
    serializer_class = LeaveReportSerializers

    def create(self, request, **kwargs):
        if self.request.user.is_verified:
            serializer = LeaveReportSerializers(data=request.data)
            if serializer.is_valid():
                serializer.save(report_initiator=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class LeaveReportDetailView(generics.RetrieveAPIView):
    queryset = LeaveReport.objects.all()
    serializer_class = LeaveReportSerializers
    lookup_field = 'pk'
    # permission_classes = [IsAdminUser,]


class LeaveReportUpdateView(generics.UpdateAPIView):
    queryset = LeaveReport.objects.all()
    serializer_class = LeaveReportSerializers
    lookup_field = 'pk'

    def update(self, request, **kwargs):
        if self.request.user.is_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = LeaveReportSerializers(instance, data=request.data, partial=partial)
            if serializer.is_valid():
                serializer.save(updated_by=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class LeaveReportDeleteView(generics.DestroyAPIView):
    queryset = LeaveReport.objects.all()
    serializer_class = LeaveReportSerializers
    lookup_field = 'pk'


class AdminReportListView(generics.ListAPIView):
    queryset = AdminReport.objects.all()
    serializer_class = AdminReportSerializers
    permission_classes = [IsAdminUser,]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['report_writer']


class AdminReportCreateView(generics.CreateAPIView):
    queryset = AdminReport.objects.all()
    serializer_class = AdminReportSerializers

    def create(self, request, **kwargs):
        if self.request.user.is_verified:
            serializer = AdminReportSerializers(data=request.data)
            if serializer.is_valid():
                serializer.save(report_writer=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class AdminReportDetailView(generics.RetrieveAPIView):
    queryset = AdminReport.objects.all()
    serializer_class = AdminReportSerializers
    # permission_classes = [IsAdminUser,]
    lookup_field = 'pk'


class AdminReportUpdateView(generics.UpdateAPIView):
    queryset = AdminReport.objects.all()
    serializer_class = AdminReportSerializers
    lookup_field = 'pk'

    def update(self, request, **kwargs):
        if self.request.user.is_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = AdminReportSerializers(instance, data=request.data, partial=partial)
            if serializer.is_valid():
                serializer.save(updated_by=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class AdminReportDeleteView(generics.DestroyAPIView):
    queryset = AdminReport.objects.all()
    serializer_class = AdminReportSerializers
    lookup_field = 'pk'


class DailyCallReportListView(generics.ListAPIView):
    queryset = DailyCallReport.objects.all()
    serializer_class = DailyCallReportSerializers
    permission_classes = [IsAdminUser,]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['client_name']


class DailyCallReportCreateView(generics.CreateAPIView):
    queryset = DailyCallReport.objects.all()
    serializer_class = DailyCallReportSerializers


class DailyCallReportDetailView(generics.RetrieveAPIView):
    queryset = DailyCallReport.objects.all()
    serializer_class = DailyCallReportSerializers
    # permission_classes = [IsAdminUser,]
    lookup_field = 'pk'


class DailyCallReportUpdateView(generics.UpdateAPIView):
    queryset = DailyCallReport.objects.all()
    serializer_class = DailyCallReportSerializers
    lookup_field = 'pk'


class DailyCallReportDeleteView(generics.DestroyAPIView):
    queryset = DailyCallReport.objects.all()
    serializer_class = DailyCallReportSerializers
    lookup_field = 'pk'                


class DcrHigherListView(generics.ListAPIView):
    queryset = DcrHigher.objects.all()
    serializer_class = DcrHigherSerializers
    permission_classes = [IsAdminUser,]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['updated_by']


class DcrHigherCreateView(generics.CreateAPIView):
    queryset = DcrHigher.objects.all()
    serializer_class = DcrHigherSerializers


class DcrHigherDetailView(generics.RetrieveAPIView):
    queryset = DcrHigher.objects.all()
    serializer_class = DcrHigherSerializers
    # permission_classes = [IsAdminUser,]
    lookup_field ='pk'


class DcrHigherUpdateView(generics.UpdateAPIView):
    queryset = DcrHigher.objects.all()
    serializer_class = DcrHigherSerializers
    lookup_field ='pk'

    def update(self, request, **kwargs):
       if self.request.user.is_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = DcrHigherSerializers(instance,data=request.data, partial=partial)
            if serializer.is_valid():
               serializer.save(updated_by=request.user)
               return Response(serializer.data, status.HTTP_201_CREATED)


class DcrHigherDeleteView(generics.DestroyAPIView):
    queryset = DcrHigher.objects.all()
    serializer_class = DcrHigherSerializers                   
    lookup_field ='pk'    


class SalesReportListView(generics.ListAPIView):
    queryset = SalesReport.objects.all()
    serializer_class = SalesReportSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['client_name']


class SalesReportCreateView(generics.CreateAPIView):
    queryset = SalesReport.objects.all()
    serializer_class = SalesReportSerializers


class SalesReportDetailView(generics.RetrieveAPIView):
    queryset = SalesReport.objects.all()
    serializer_class = SalesReportSerializers
    lookup_field = 'pk'
    # permission_classes = [IsAdminUser, ]


class SalesReportUpdateView(generics.UpdateAPIView):
    queryset = SalesReport.objects.all()
    serializer_class = SalesReportSerializers
    lookup_field = 'pk'


class SalesReportDeleteView(generics.DestroyAPIView):
    queryset = SalesReport.objects.all()
    serializer_class = SalesReportSerializers
    lookup_field = 'pk'                
