from salary_Expenses.models import *
from salary_Expenses.serializers import *
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import filters, status
from rest_framework.permissions import IsAdminUser
import django_filters.rest_framework

# Create your views here.

class SalaryExpensesListView(generics.ListAPIView):
    queryset = SalaryExpenses.objects.all()
    serializer_class = SalaryExpensesSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['salary']


class SalaryExpensesCreateView(generics.CreateAPIView):
    queryset = SalaryExpenses.objects.all()
    serializer_class = SalaryExpensesSerializers

    def create(self, request, **kwargs):
        if self.request.user.is_verified:
            serializer = SalaryExpensesSerializers(data=request.data)
            if serializer.is_valid():
                serializer.save(created_by=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class SalaryExpensesDetailView(generics.RetrieveAPIView):
    queryset = SalaryExpenses.objects.all()
    serializer_class = SalaryExpensesSerializers
    # permission_classes = [IsAdminUser, ]
    lookup_field = 'pk'


class SalaryExpensesUpdateView(generics.UpdateAPIView):
    queryset = SalaryExpenses.objects.all()
    serializer_class = SalaryExpensesSerializers
    lookup_field = 'pk'

    def update(self, request, **kwargs):
        if self.request.user.is_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            print(instance)
            serializer = SalaryExpensesSerializers(instance, data=request.data, partial=partial)
            if serializer.is_valid():
                serializer.save(updated_by=request.user)
                return Response(serializer.data, status.HTTP_201_CREATED)


class SalaryExpensesDeleteView(generics.DestroyAPIView):
    queryset = SalaryExpenses.objects.all()
    serializer_class = SalaryExpensesSerializers
    lookup_field = 'pk'
