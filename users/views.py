from django.shortcuts import render
from users.serializers import *
from users.models import *
from rest_framework import generics
from rest_framework.permissions import IsAdminUser
from rest_framework import status
from rest_framework.response import Response
import django_filters.rest_framework


# Create your views here.

def home(request):
    return render(request, 'index.html')

# Designation class
class DesignationListView(generics.ListAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['designation_value']


class DesignationCreateView(generics.CreateAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerializers

class DesignationDetailView(generics.RetrieveAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerializers
    permission_classes = [IsAdminUser, ]
    lookup_field = 'pk'


class DesignationUpdateView(generics.UpdateAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerializers
    lookup_field = 'pk'


class DesignationDeleteView(generics.DestroyAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerializers
    lookup_field = 'pk'


# User class
class UserListView(generics.ListAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['user_email']


class UserCreateView(generics.CreateAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserSerializers
    permission_classes = [IsAdminUser, ]


class UserUpdateView(generics.UpdateAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserSerializers
    lookup_field = 'pk'


class UserDetailView(generics.RetrieveAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserSerializers
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, ]


class UserDeleteView(generics.DestroyAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserSerializers
    lookup_field = 'pk'


# UserControlTable class
class UserControlTableListView(generics.ListAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserControlTableSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class UserControlTableCreateView(generics.CreateAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserControlTableSerializers
    permission_classes = [IsAdminUser, ]


class UserControlTableUpdateView(generics.UpdateAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserControlTableSerializers
    lookup_field = 'pk'


class UserControlTableDetailView(generics.RetrieveAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserControlTableSerializers
    lookup_field = 'pk'


class UserControlTableDeleteView(generics.DestroyAPIView):
    queryset = UserData.objects.all()
    serializer_class = UserControlTableSerializers
    lookup_field = 'pk'


# ClientType class
class ClientTypeListView(generics.ListAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTypeSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['client_type_name']


class ClientTypeCreateView(generics.CreateAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTypeSerializers
    permission_classes = [IsAdminUser, ]


class ClientTypeUpdateView(generics.UpdateAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTypeSerializers
    lookup_field = 'pk'


class ClientTypeDetailView(generics.RetrieveAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTypeSerializers
    lookup_field = 'pk'


class ClientTypeDeleteView(generics.DestroyAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTypeSerializers
    lookup_field = 'pk'


# ClientTable class
class ClientTableListView(generics.ListAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTableSerializers
    permission_classes = [IsAdminUser, ]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    search_fields = ['client_name']


class ClientTableCreateView(generics.CreateAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTableSerializers
    permission_classes = [IsAdminUser, ]

    def create(self, request, **kwargs):
        if self.request.user.is_verified:
            serializer = ClientTableSerializers(data=request.data)
            if serializer.is_valid():
                serializer.save(user_id=request.user)
                return Response(serializer.data)
            return Response(serializer.data, status.HTTP_201_CREATED)


class ClientTableUpdateView(generics.UpdateAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTableSerializers
    lookup_field = 'pk'

    def update(self, request, **kwargs):
        if self.request.user.is_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = ClientTableSerializers(instance, data=request.data, partial=partial)
            if serializer.is_valid():
                serializer.save(updated_by=request.user)
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClientTableDetailView(generics.RetrieveAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTableSerializers
    lookup_field = 'pk'


class ClientTableDeleteView(generics.DestroyAPIView):
    queryset = UserData.objects.all()
    serializer_class = ClientTableSerializers
    lookup_field = 'pk'
